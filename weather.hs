import System.Environment
import Data.List
import Network.HTTP.Conduit
import Control.Monad.IO.Class
import Data.ByteString.Lazy
import Data.Aeson
import Data.Attoparsec.Number
import Control.Applicative
import Control.Monad.Trans

main = do
  args <- getArgs
  putStrLn "That arguments are:"
  putStrLn $ head args
