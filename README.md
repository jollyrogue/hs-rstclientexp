# hs-rstclientexp

REST client experiment in haskell.

The objective is to get the weather for a given location from metaweather.com.

## Links:
- https://www.metaweather.com/api/#locationsearch

## Usage:
### Location Search
weather search [-w|--woeid] <location|lat,long>

#### Output
Where on Earth ID (WOEID):
Title:
Location Type:
Lat,Long:
Distance: 

### 5 Day Weather 
weather <location woeid>

### Weather on Specific Day
weather day <location woeid> <yyyy/mm/dd>
